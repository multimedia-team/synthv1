Source: synthv1
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Dennis Braun <snd@debian.org>,
 Alessio Treglia <alessio@debian.org>,
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 ladspa-sdk,
 libasound2-dev,
 libjack-dev | libjack-jackd2-dev,
 liblo-dev,
 libqt6svg6-dev,
 libsndfile1-dev,
 libx11-dev,
 lv2-dev,
 qt6-base-dev,
 qt6-tools-dev-tools
Homepage: https://synthv1.sourceforge.io/
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/multimedia-team/synthv1.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/synthv1
Rules-Requires-Root: no

Package: synthv1
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 synthv1-common (=${source:Version}),
Recommends:
 jackd
Description: old-school polyphonic synthesizer - standalone
 synthv1 is an old-school all-digital 4-oscillator subtractive
 polyphonic synthesizer with stereo effects, especially suited
 to create strong bass sounds.
 .
 This package provides the standalone app.

Package: synthv1-lv2
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 synthv1-common (=${source:Version}),
Provides:
 lv2-plugin
Description: old-school polyphonic synthesizer - LV2 plugin
 synthv1 is an old-school all-digital 4-oscillator subtractive
 polyphonic synthesizer with stereo effects, especially suited
 to create strong bass sounds.
 .
 This package provides the LV2 plugin.

Package: synthv1-common
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: old-school polyphonic synthesizer - common files
 synthv1 is an old-school all-digital 4-oscillator subtractive
 polyphonic synthesizer with stereo effects, especially suited
 to create strong bass sounds.
 .
 This package provides files shared by both the LV2 plugin
 and the standalone application.
